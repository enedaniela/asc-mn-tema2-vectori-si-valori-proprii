function [valp vecp] = PutereInv(d, s, h, y, maxIter, eps)
	
	for k = 1:maxIter
%rezolvarea sistemului (A - hI)*z = y folosind algoritmul lui Thomas
		z = Thomas(s, d-h, s, y);
		y = z/norm(z);	
		y = y';
%functia produs este echivalentul inmultirii y'*A
		p = produs(d,s,y)';
		valp = p*y;
		h = valp;
%conditie de oprire
		if norm(produs(d,s,y)  - valp*y) < eps
			break
		end
	end
	vecp = y;
endfunction
