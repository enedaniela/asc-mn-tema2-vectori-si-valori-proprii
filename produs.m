function v = produs(d,s,a)
	
	n = length(d);
	y = d'.*a;
	
	u(2:n,1) = s'.*a(1:n-1,1);
	
	w = s'.*a(2:n,1);
	w(n) = 0;
	v = y + u + w;
endfunction
