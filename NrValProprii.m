function numvp = NrValProprii(d, s, val_lambda)
	
	P = ValoriPolinoame(d, s, val_lambda);
	n = length(d);
	numvp = 0;
	for i = 1:n
%daca P(i) = 0, P(i) are semnul opus al lui P(i+1)
		if P(i) == 0
			P(i) = -P(i+1);
		endif
%se calculeaza numrul schimbarilor de semn
		if P(i)*P(i+1) < 0
			numvp++;
		endif

	endfor
endfunction