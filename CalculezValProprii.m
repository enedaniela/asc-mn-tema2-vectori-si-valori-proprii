function vp = CalculezValProprii(d, s, m, eps)
	n = length(d);
	%se reinitializeaza m, in cazul in care depaseste dimensiunea matricei
	if( m > n)
		m = n;
	end
	r = IntervaleValProprii(d, s, m);
	
	for i = 1:m
		%metoda bisectiei
		a = r(i);
		b = r(i+1);
		p0 = (a+b)/2;
		%se foloseste ultima valoarea din vectorul returnat de ValoriPolinoame(d, s, p0) ca functie pentru metoda bisectiei
		while( abs(ValoriPolinoame(d,s,p0)(n+1)) > eps)
			
			if ValoriPolinoame(d,s,r(i))(n+1)*ValoriPolinoame(d,s,p0)(n+1) < 0
				b = p0;
			end
			if ValoriPolinoame(d,s,r(i+1))(n+1)*ValoriPolinoame(d,s,p0)(n+1) < 0 
				a = p0;
			end
				p0 = (a+b)/2;
		end
		vp(i) = p0;
	end
endfunction

