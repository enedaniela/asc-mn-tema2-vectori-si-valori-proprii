function [limita_inf, limita_sup] = LimiteValProprii(d, s)

	n = length(d);
	t = zeros(n);
%Teorema lui Gershgorin
	t(1) = abs(s(1));

	for i = 2:n-1
		t(i) = abs(s(i - 1)) + abs(s(i));
	endfor

	t(n) = abs(s(n-1));

	for i = 1:n
		limita_inf(i) = min(d(i) - t(i));
		limita_sup(i) = max(d(i) + t(i));
	endfor
	limita_inf = min(limita_inf);
	limita_sup = max(limita_sup);

endfunction
