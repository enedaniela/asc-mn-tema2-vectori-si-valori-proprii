function r1 = IntervaleValProprii(d, s, m)

	n = length(d);
	%se reinitializeaza m, in cazul in care depaseste dimensiunea matricei
	if( m > n)
		m = n;
	end
	%se extrag limita inferioara si cea superioara folosind functia LimiteValProprii(d, s)
	[limita_inf, limita_sup] = LimiteValProprii(d, s);
	r(1) = limita_inf;
	r(m+2) = limita_sup;
	for k = m:-1:1
	%se calculeaza mijlocul intervalului 
		mij = (r(k+2) + r(1))/2;
	%se calculeaza lungimea intervalului
		h = r(k+2) - mij;
	%se calculeaza numarul de valori proprii mai mici decat valoarea mij
		numvp = NrValProprii(d, s, mij);
	%se actualizeaza valoarea mij
		h = h/2;
		if numvp < k
			mij = mij + h;
		else if numvp > k
			mij = mij - h;
			end
		end
		while numvp ~= k
			numvp = NrValProprii(d, s, mij);
			h = h/2;
		if numvp < k
			mij = mij + h;
		else if numvp > k
			mij = mij -h;
			end
		end
		endwhile
		r(k+1) = mij;
	endfor
	r1 = r(1:m+1);
endfunction
